const Sqlite = require("better-sqlite3");

let db = new Sqlite("db.sqlite");

db.prepare('DROP TABLE IF EXISTS user').run();

db.prepare("CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT UNIQUE NOT NULL,password TEXT NOT NULL)").run();