let express = require('express');
let mustache = require('mustache-express');
let app = express();
const fs = require('fs');

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

app.use(express.static('public'))
app.use('/image',express.static('image'))
app.use('/css',express.static('css'))
app.use('/js',express.static('js'))

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
const session = require('express-session');
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}));
const Sqlite = require('better-sqlite3');

const db = new Sqlite('db.sqlite');

const createDbSql = fs.readFileSync('create-db.js', 'utf8');


app.get('/',(req, res) => {
  res.render('Accueil');
});

app.get('/Accueil.html', function(req, res) {
  res.render('Accueil');
});

app.get('/Service.html', function(req, res) {
  res.render('Service');
});

app.get('/Service1.html', function(req, res) {
  res.render('Service1');
});

app.get('/Service2.html', function(req, res) {
  res.render('Service2');
});

app.get('/Service3.html', function(req, res) {
  res.render('Service3');
});

app.get('/Propos.html', function(req, res) {
  res.render('Propos');
});

app.get('/Equipe.html', function(req, res) {
  res.render('Equipe');
});

app.get('/histoire.html', function(req, res) {
  res.render('histoire');
});

app.get('/partenaires.html', function(req, res) {
  res.render('partenaires');
});

app.get('/Contact.html', function(req, res) {
  res.render('Contact');
});

app.get('/Formulaire.html', function(req, res) {
  res.render('Formulaire');
});

app.get('/Tarifs.html', function(req, res) {
res.render('Tarifs');
});

app.get('/Connexion.html', function(req, res) {
  res.render('Connexion');
});

app.get('/Inscription.html', function(req, res) {
  res.render('Inscription');
});

app.get('/Paiement.html', function(req, res) {
  res.render('Paiement');
});

app.get('/Payer.html', function(req, res) {
  res.render('Payer');
});

// Inscription

const bcrypt = require('bcrypt');
app.post('/inscription', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const select = db.prepare('SELECT id FROM user WHERE username = ?');
  const user = select.get(username);
  if (user) {
      throw new Error("Cet identifiant est déjà utilisé.");
  } else {
      const hashedPassword = bcrypt.hashSync(password, 10);
      const insert = db.prepare('INSERT INTO user(username, password) VALUES (?, ?)');
      insert.run(username, hashedPassword);
      res.redirect("/");
  }
});

// Connexion


app.post('/connexion', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  const select = db.prepare('SELECT id, password FROM user WHERE username = ?');
  const user = select.get(username);
  if (user) {
    const testPassword = bcrypt.compareSync(password, user.password);
    if (!testPassword) {
      throw new Error('Mot de passe incorrect');
    } else {
      req.session.username = username;
    }
  } else {
    throw new Error('Utilisateur introuvable');
  }
  res.redirect("/");
});

// req.session.username c'est l'username avec lequel tu t'es connecté, tu peux l'utiliser partout ça te renvoie ton username




















app.listen(3000, () => console.log('Serveur lancé sur le port http://localhost:3000'));