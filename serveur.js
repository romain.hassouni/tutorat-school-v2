"use strict";

let express = require('express');
let mustache = require('mustache-express');
let app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

app.use(express.static('public'))
app.use('/image',express.static('image'))
app.use('/css',express.static('css'))
app.use('/js',express.static('js'))

let users = require('./tutorat_sql');
let db_filename = 'tutorat.json';
users.load(db_filename);  

const cookieParser = require('cookie-parser');

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('data2.db');

app.use(cookieParser());

app.get('/', (req, res) => {
  res.cookie('connect.sid', 'example_cookie_value', { sameSite: 'none', secure: true });
  res.send('Cookie set with SameSite=None');
});


app.get('/',(req, res) => {
  res.render('Accueil');
});

app.get('/Accueil.html', function(req, res) {
  res.render('Accueil');
});

app.get('/Service.html', function(req, res) {
  res.render('Service');
});

app.get('/Service1.html', function(req, res) {
  res.render('Service1');
});

app.get('/Service2.html', function(req, res) {
  res.render('Service2');
});

app.get('/Service3.html', function(req, res) {
  res.render('Service3');
});

app.get('/Propos', function(req, res) {
  res.render('Propos');
});

app.get('/Equipe.html', function(req, res) {
  res.render('Equipe');
});

app.get('/histoire.html', function(req, res) {
  res.render('histoire');
});

app.get('/partenaires.html', function(req, res) {
  res.render('partenaires');
});

app.get('/Contact.html', function(req, res) {
  res.render('Contact');
});

app.get('/Formulaire.html', function(req, res) {
  res.render('Formulaire');
});

app.get('/Coordonnees.html', function(req, res) {
res.render('Coordonnees');
});

app.get('/Connexion.html', function(req, res) {
  res.render('Connexion');
});
app.get('/Inscription.html', function(req, res) {
  res.render('Inscription');
});

function register(req, res, username, password) {
  const select = db.prepare('SELECT id FROM users WHERE username = ?');
  const user = select.get(username);

  const hashedPassword = bcrypt.hashSync(password, 20);
  const insert = db.prepare('INSERT INTO users (username, password) VALUES (?, ?)');
  insert.run(username, hashedPassword);
  res.render('Accueil');
  
}
const bcrypt=require('bcrypt');

app.post('/register', function(req, res) {
  const username=req.body.username;
  const password=req.body.password;
  register(req,res,username,password);

});



app.get('/deconnexion', function(req, res) {
  // Supprimer l'utilisateur de la session
  delete req.session.user;

  // Rediriger vers la page d'accueil
  res.redirect('/');
});


app.listen(3000, () => console.log('Tutorat server at http://localhost:3000'));